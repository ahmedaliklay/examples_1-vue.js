var app = new Vue({
    el: '#app',
    data: {
        isSelected: false,
    },
   methods:{
    secondMethod:function(event){
        event.target.classList.toggle('selected');
    },
    thirdMethod:function(event,className){
        event.target.classList.toggle(className);
    }
   }
})