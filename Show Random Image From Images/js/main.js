var photos = [
    '../img/img_1.jpg',
    '../img/img_2.jpg',
    '../img/img_3.jpg',
    '../img/img_4.jpg',
    '../img/img_5.jpg',
    '../img/img_6.jpg',
    '../img/img_7.jpg',
    '../img/img_8.jpg',
    '../img/img_9.jpg',
    '../img/img_10.jpg',
    '../img/img_12.jpg',
    '../img/img_13.jpg',
    '../img/img_14.jpg',
    '../img/img_15.jpg',
    '../img/img_16.jpg',
];
var app = new Vue({
    el: '#app',
    data: {
        photos: photos,
        selected: null
    },
    methods:{
        randomPhoto:function(imgs){
            return imgs[Math.floor(Math.random() * photos.length)];
        }
    },
    created(){
        this.selected = this.randomPhoto(this.photos);
    }
})