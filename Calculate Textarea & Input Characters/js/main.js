var app = new Vue({
    el: '#app',
    data: {
        maxLength: 20,
        remainingChars:20,
        message:null,
        reachZero:false
    },
    methods:{
       countChars:function(){
           this.remainingChars = this.maxLength - this.message.length;
            this.reachZero = this.remainingChars === 0;
       }
    }
})