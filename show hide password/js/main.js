var app = new Vue({
    el: '#app',
    data: {
        name:'Ahmed Ali Klay',
        fieldType:'password'
    },
    methods:{
        switchField(){
            this.fieldType = this.fieldType  === 'password' ? 'text' : 'password'
        }
    }
})