var app = new Vue({
    el: '#app',
    data: {
        formError: [],
        userName:null,
        subject:null,
        msg:null,
        maxChar:10
    },
    methods:{
        validateForm:function(e){
            this.formError = []; //Empty form errors
            //check if username is empty
            if(!this.userName){
                this.formError.push('UserName Can Not Be Empty');
            }
            //check if subject is empty
            if(!this.subject){
                this.formError.push('subject Can Not Be Empty');
            }
            //check if msg is empty
            if(!this.msg){
                this.formError.push('msg Can Not Be Empty');
            }
            //check UserName characters count
            if(this.userName && this.userName.length > this.maxChar){
                this.formError.push(`this is field can not be more than ${this.maxChar}  characters`);
            }
            //if  No Errors Return True
            if(!this.formError.length){
                return true;
            }
            e.preventDefault();
        }
    }
})