var app = new Vue({
    el: '#app',
    data: {
        amount: 1,
        price:20
    },
    computed:{
        totalPrice:function(){
            return this.amount * this.price;
        }
    }
})