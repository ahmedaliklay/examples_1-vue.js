var app = new Vue({
    el: '#app',
    data: {
        convert:'',
        output:''
    },
    methods:{
        convertTile(){
            //Save Var
            var convert = this.convert;
            
            //Convert To Lower Case
            convert = convert.toLowerCase();

            //delete and convert space to -
            convert = convert.replace(/\s+/g,'-');

            //convert Ampersand with dash and space
            convert = convert.replace(/&/g,'and');

            this.output = convert;
        }
    }
})